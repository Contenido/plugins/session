package session::Init;

use strict;
use warnings 'all';

use session::Apache;
use session::Keeper;
use session::Session;
use session::AUTH::FaceBook;
use session::AUTH::VKontakte;
use session::AUTH::Mailru;
use session::AUTH::Google;

# загрузка всех необходимых плагину классов
# session::SQL::SomeTable
# session::SomeClass
Contenido::Init::load_classes(qw(
	));

sub init {
	0;
}

1;
