package session::Apache;

use strict;
use warnings 'all';

use session::State;
use Contenido::Globals;


sub child_init {
	# встраиваем keeper плагина в keeper проекта
	$keeper->{session} = session::Keeper->new($state->session);
}

sub request_init {
}

sub child_exit {
}

1;
