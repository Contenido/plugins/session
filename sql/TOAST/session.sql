CREATE TABLE sessions (
  id char(32) not null primary key,
  dtime timestamp not null default now(),
  a_session text
);
