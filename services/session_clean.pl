#!/usr/bin/perl

use strict;
use warnings "all";
use locale;

use FindBin;
BEGIN {
        require "$FindBin::RealBin/../lib/Modules.pm";
}

use Contenido::Globals;
use Contenido::Init;
use ErrorTee;
use PidFile;


# begin
Contenido::Init->init();

my $keeper_module = $state->project.'::Keeper';
$keeper = $keeper_module->new($state);

#PidFile->new($keeper, compat=>1);                # db-based locking (run only on one host)
#PidFile->new($keeper, compat=>1, per_host=>1);   # db-based locking (run on whole cluster)

my $session_lifetime;
for my $plugin ( split(/\s+/, $state->plugins) ) {
	my $class = $plugin.'::Apache';
	eval { $class->child_init(); };
	if ( $@ ) {
		$log->error("Не могу выполнить метод child_init плагина $plugin ($class) по причине '$@'");
	} elsif ( $plugin eq 'session' ) {
		$session_lifetime = $state->{session}->lifetime;
	}
}

############################################
# please use:
#     $state->{log_dir} for logging
#     $state->{tmp_dir} for temporary files
###########################################

print "Lifetime: $session_lifetime\n"                   if $DEBUG;
if ( $session_lifetime ) {
	my $DBH = $keeper->SQL;
	my $now = Contenido::DateTime->new;
	my $then = $now->clone->subtract( seconds => $session_lifetime );
	my $request = "DELETE FROM sessions WHERE dtime < ?";
	my $sth = $DBH->prepare($request);
	print $then->ymd('-').' '.$then->hms."\n"       if $DEBUG;
	$sth->execute( $then->ymd('-').' '.$then->hms );
	$sth->finish;
}
